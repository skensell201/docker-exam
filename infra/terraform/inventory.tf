data "template_file" "inventory" {
  template = "${file("inventory.tpl")}"
  vars = {
    vps1 = digitalocean_droplet.vps[0].ipv4_address
  }
}

resource "local_file" "file" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "../ansible/inventory"
}
