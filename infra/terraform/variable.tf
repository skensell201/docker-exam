variable "DO_TOKEN" {
  description = "The value is taken from the environment variable TF_VAR_DO_TOKEN"
}

variable "DO_NAME_SSH" {
  description = "The value is taken from the environment variable TF_VAR_DO_NAME_SSH"
}

variable "DO_NAME_SSH_ANOTHER" {
  description = "The value is taken from the environment variable TF_VAR_DO_NAME_SSH_ANOTHER"
}

variable "DO_EMAIL" {
  description = "The value is taken from the environment variable TF_VAR_DO_EMAIL. Must be kind name_at_example_com -> (name@example.com)"
}

variable "DO_MODULE" {
  description = "The value is taken from the environment variable TF_VAR_DO_MODULE"
}
variable "servers" {
  default = ["skensel-vps"]
}

variable "DNS_RECORD_HIDDEN" {
  description = "The value is taken from the environment variable TF_VAR_DNS_RECORD_HIDDEN"
}
variable "aws_region" {
  type = string
  default = "us-east-1"
}
variable "AWS_ACCESS_KEY" {
  description = "The value is taken from the environment variable TF_VAR_AWS_ACCESS_KEY"
}
variable "AWS_SECRET_KEY" {
  description = "The value is taken from the environment variable TF_VAR_AWS_SECRET_KEY"
}
