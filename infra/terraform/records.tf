provider "aws" {
  region  = var.aws_region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

data "aws_route53_zone" "dns_record" {
  name = var.DNS_RECORD_HIDDEN
  private_zone = false
}

resource "aws_route53_record" "www" {
  depends_on = [digitalocean_droplet.vps]
  zone_id = data.aws_route53_zone.dns_record.zone_id
  name    = "final-docker.skensel.${data.aws_route53_zone.dns_record.name}"
  type    = "A"
  ttl     = "300"
  records = [digitalocean_droplet.vps[0].ipv4_address]
}
