#################################################################
# Creates a docker image with Nginx Unit
#                    ##        .
#              ## ## ##       ==
#           ## ## ## ##      ===
#       /""""""""""""""""\___/ ===
#  ~~~ {~~ ~~~~ ~~~ ~~~~ ~~ ~ /  ===- ~~~
#       \______ o          __/
#         \    \        __/
#          \____\______/
# Author: Ivan Kostin <skense1@yandex.ru>
#################################################################
FROM nginx/unit

RUN apt update \
    && apt install -y php7.3-pgsql \
    && mkdir -p /speedtest/

# Copy sources
COPY backend/ /speedtest/backend
COPY results/*.php /speedtest/results/
COPY results/*.ttf /speedtest/results/
COPY *.js /speedtest/
COPY docker/config/speedtest.json /speedtest.json
COPY docker/*.php /speedtest/
COPY docker/entrypoint.sh /

# Final touches
EXPOSE 80
CMD ["bash", "/entrypoint.sh"]
