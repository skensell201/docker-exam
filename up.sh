#!/usr/bin/env bash

########### MAIN APPLICATION #########

# If the services is enabled, it will be stopped
#docker-compose -f app.compose.yml down
#docker-compose -f efk.compose.yml down

# Start services EFK
docker-compose -f efk.compose.yml up -d

# Start services APP
docker-compose -f app.compose.yml up -d

# Migrate database
#docker exec -i postgres psql -U postgres < results/telemetry_postgresql.sql

# Add config for Nginx Unit
docker-compose -f app.compose.yml exec app \
                  curl -X PUT --data-binary @speedtest.json \
                  --unix-socket var/run/control.unit.sock http://localhost/config


